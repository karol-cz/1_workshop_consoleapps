package package1;

import java.util.Random;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) {

		Random rd = new Random();
		int randNumber = rd.nextInt(99) + 1;

		Scanner sc = new Scanner(System.in);
		int number;

		do {
			System.out.print("Guess a number: ");
			while (!sc.hasNextInt()) {
				System.out.print("It is not a number ");
				sc.next();
			}

			number = sc.nextInt();

			if (number > randNumber) {
				System.out.println("Too big!");
			} else if (number < randNumber) {
				System.out.println("Too small!");
			} else {
				System.out.println("You won!");
			}
		} while (number != randNumber);

	}

}
