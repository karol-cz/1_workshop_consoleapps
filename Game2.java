package package1;

import java.util.Scanner;

public class Game2 {

	public static void main(String[] args) {

		System.out.println("Think of a number between 0 and 1000 and I will guess it in max 10 tries");

		int min = 0;
		int max = 1000;

		askingLoop: do {
			int guess = (int) (max - min) / 2 + min;
			System.out.println("My guess is: " + guess);

			Scanner sc = new Scanner(System.in);
			String answer = sc.nextLine();

			if (answer.equals("correct")) {
				System.out.println("Wygrałem");
				break;
			} else if (answer.equals("to big")) {
				max = guess;
				continue askingLoop;
			} else if (answer.equals("to small")) {
				min = guess;
				continue askingLoop;
			} else {
				System.out.println("don't cheat!");
			}
		} while (true);
	}
}
