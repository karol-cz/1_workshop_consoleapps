package package1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

/**
 * LOTTO class simulates a Lottery. Application takes 6 numbers from console
 * and they are compared with 6 numbers drawn from pool of 1-49 
 */
public class LOTTO {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		Integer[] arrUser = new Integer[6];
		Integer[] arr = new Integer[49];

		// filling the pool of numbers
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
		}

		for (int i = 0; i < arrUser.length; i++) {
			loop: do {
				System.out.print("Pick a number " + (i + 1) + ": ");

				if (!sc.hasNextInt()) {
					String in = sc.next();
					System.out.println("'" + in + "' It is not a number ");
					continue;
				}
				int t = sc.nextInt();

				if (t < 1 || t > 49) {
					System.out.println("Ypu picked number which is out of range 1-49, ");
					continue;
				}

				for (int j = 0; j < i; j++) {
					if (arrUser[j] == t) {
						System.out.println("You've already picked that number");
						continue loop;
					}
				}
				arrUser[i] = t;
				break;
			} while (true);

		}

		System.out.println(Arrays.toString(arrUser));
		Arrays.sort(arrUser);
		System.out.println(Arrays.toString(arrUser));

		Collections.shuffle(Arrays.asList(arr));
		Integer[] arrResults = Arrays.copyOf(arr, 6);
		Arrays.sort(arrResults);

		System.out.println("Lucky numbers are: " + "\n" + Arrays.toString(arrResults));

		int wins = 0;
		for (int i : arrResults) {
			for (int j : arrUser) {
				if (i == j) {
					wins++;
				}
			}
		}

		if (wins >= 3) {
			System.out.println("Number of wins: " + wins);
		} else {
			System.out.println("Try once again");
		}

		/*
		 * CIEKAWOSTKA
		 * 1) How many draws you need to make to draw 6 numbers correctly?
		 * 2) How much you need to invest to win?:)
		 * 3) How much you can earn?:)
		 */
		Random rd = new Random();

		int numberOfTries = 0;
		int[] eachCount = new int[7];
		while (true) {
			wins = 0;
			for (int i = 0; i < 6; i++) {
				stepBack: while (true) {
					int randomNumber = rd.nextInt(49) + 1;
					for (int j = 0; j < i; j++) {
						if (arrResults[j] == randomNumber)
							continue stepBack;
					}
					arrResults[i] = randomNumber;
					break;
				}
			}

			for (int i : arrResults) {
				for (int j : arrUser) {
					if (i == j) {
						wins++;
					}
				}
			}
			numberOfTries++;
			eachCount[wins]++;
			if (wins == 6) {
				break;
			}
		}
		System.out.println("Your numbers will be lucky numbers after: " + numberOfTries + " number of tries");
		System.out.println("Money spent on all tries: " + numberOfTries*3 + "$");
		System.out.println("Total wins: " 
				+ (eachCount[3]*10 + eachCount[4]*100 + eachCount[5]*3500 + 1_000_000) + "$");
		
	}

}
